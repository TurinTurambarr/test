import { GoogleSpreadsheet, SheetType } from 'google-spreadsheet';

export interface INotification {
    name?: string;
    description?: string;
    mounth?: number;
    year?: number;
    day?: number
}

export interface IStorage {
    userId: number|null;
    notificationId: string[],
    message: string,
    keyboard?: object[],
    group?: any[],
    spreadSheet: null|SheetType,
    nootification: INotification
}

export const storage: IStorage = {
    userId: null,
    notificationId: [],
    message: '',
    keyboard: [],
    group: [],
    spreadSheet: null,
    nootification: {}
}