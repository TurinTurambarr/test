export const schema = {
    TRACING: {
        type: Boolean,
        optional: true,
    },
    NODE_ENV: {
        type: String,
        default: 'local',
    }
} as const;

export const Role: object = {
    ADMIN: 'admin'
};
export const BOT_TOKEN: string = '5453713169:AAFiVo3qQdfRYwZ_lEU48hUEZNP342li_aw';
export const ConfigGoogleTable: object = {
    "type": "service_account",
    "project_id": "astral-field-354917",
    "private_key_id": "88879c74798eb871dbc8f76cb0628eb1dabbcc47",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC3Dn8MW3lf1UNK\nBAwAdWjVFVEGWbdL1mmbHmYrKVab1GjVLcv96VfKXM6ztohz4kzKSS8f+VhhVHX8\n7rkgO1EH8M8/5q4FByT8DEf7DMrsYAJkc1MoNVpNVM4RwlAsr1tLTBILHHAXzc0e\ncIdIkNuUM7poIkT15yc8BE9crEjriwUW0FJDZUtb5TjZWaQ7j5H1K8Q3Xww5VaUG\n5gx3lp1YIpJgP71u5iOnT9Yx2//o3kY2kOxp8MgEFwCfwsQcBqeFY0/TuaKVODx4\nhK1o+aXsF/6OMcbE5PGeFuDy91hVvOwCPsGnVsvkl3u87jtwzvbfQz/kCYBTnRi/\nsD7hy4PlAgMBAAECggEALdIamijBgmv6+ml16Z15/rBlxi2p1HlYmOPslgw0ajAq\nh4X9pFKGz5gYg2KC2GMCbrYP7jR3W3GNUSEeRMOkPlUYQ3oGACswJkkasWwb1TwZ\nDTTS0WM9ddLu/AIEGJNSSlBByfc11beFiwOG0XK1C+1A0TUWxuiz6PF5Pgy/+fk3\nf0qzl9SwbnNCJaTE8j+opNaJvvP3nXejRQgacqXDhtl9lgLCz0G30ZvK/byGm7VZ\nrzPfsgmteDh3B9RWI9QZcdzYjdyw/jfKV9mYx/JvuVsazZ3bYNb3hC0JK8wWao9d\neerYCrpE4cHc6icY1RXvlGWE5pSvK3TlBv8t+6uR1wKBgQDfyn9AkwrlGViUi5KP\nctVMFwI22lJncqZo7/Dqd9lpmY9R6N83kehbHQLQzUOs/IjzkRLaXPtC4+EuKCYj\nnDkt/ugR44QU0n4VAOMKybcBzV+BZhCywJrutEgMYBGwwUuT6cWHC6e8poSlbPbf\nWBOWp8w97wq9jn9LP+UrGKCfGwKBgQDRZyetaFhV3z2xNAmpGSYXwvGK99o0qNvG\n39NM6v0AnIAY3A7iuyyBRdKCPH0aQ4uNtF0TBv/vwnYc3poraKXvGPkn8GyAvULF\ndnY//q01TQxVqeTfUmGd7ZVpk9ZRo2FvC/3yQpJb/2HwSZVbjC1TDlnIIR5Jq1fM\n/GRWxk+Y/wKBgBeIyBEWZZLqM3KLqwIC5YN29/ikxP20BOQMN52wtOByEaxffPnv\nEixixKUIsRFEDXLv0re8B4BlToz4E+qyxyCbKfSXpJA8Ap5a1YWQ6Pn7iwuknCTw\nNFreAqWqhLG94iDP+MHW6/De4hwzdVLPhF/XjZcFyG9eyOw07/i0Qk/5AoGAMJ8E\nDnaI9D9mgquzhojpWGESVpMgHlQK282CsXxqjGbKM3t9FVYiik2dCFJlNeDzGU8E\nwXNBn/kGFcP2BhhBVxf0Wjqv5uCLDXBpGojWzOZ787QECXqGYfYVWDNUcxY9fFxL\ndaHRe/z7T6C2RlBxAbT2UcqmzrIzK8x6u89GEh8CgYEAjgemlGW5O+7ImMPPR+YQ\ndTYFsykvZ2mw7VBlhaaGmMh7bmJ8y4v9W2sx/mLDJY4AsEViO/ATt4PJdFV/92l6\nxK3c5l+MPJY8naocrRt3tSWhHLa/n1x+qxggdeTXPjUu6cmLJ9GUcGKWVH1ZpS8e\nuecsQXx9TS5bK1i/Jcuw4s4=\n-----END PRIVATE KEY-----\n",
    "client_email": "rave-310@astral-field-354917.iam.gserviceaccount.com",
    "client_id": "109312576213310976748",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/rave-310%40astral-field-354917.iam.gserviceaccount.com"
}
export const GOOGLE_TABLE_LINK = `https://docs.google.com/spreadsheets/d/1JUrAZQ7W1WeoWLXbD55KKddYaoOzR7MenZdxtrnoDTo/edit#gid=67410950`;
export const CARD_NUMBER = `4279380770449652`;
