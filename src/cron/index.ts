import cron from 'node-cron';
import {storage} from '../config/storage';

import {PaymentsController} from '../command/controller/PaymentsController'
import {NotificationController} from '../command/controller/NotificationController'

// каждый 3 день в 9
const THREE_DAY = '0 9 */3 * *';
const UPDATE = '0 12 * * *';
const ONE = '* * * * *'

export class SheduleTimeManager {
    readonly time: string;
    private bot: object;

    constructor({time = THREE_DAY, bot} = {}) {
       this.time = time;
       this.bot = bot;
    }

    async searchDebtors() {
      const {bot} = this;
      const instancePaymentController =  new PaymentsController();
      await instancePaymentController.init()
      await instancePaymentController.searchForDebts(bot);
    }

    init() {
        const {time, bot} = this;

        cron.schedule(time, () => {
            ['searchDebtors'].forEach((method) => {
                this[method]()
            })
        });
        
        cron.schedule(UPDATE, () => {
            storage.spreadSheet.init()
            NotificationController.checkNotification(bot)
            NotificationController.checkOldNotification(bot)
        })

    // test cron
    // cron.schedule(ONE, () => {
    //
    // })
    }
    
}
