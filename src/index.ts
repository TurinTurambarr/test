import {Context, session, Telegraf} from 'telegraf';
import {GoogleSpreadsheetService} from './service/GoogleSpreadsheetService'
import {Update} from 'typegram';
import {BOT_TOKEN} from './config/constatns';
import {sequelize} from './config/db';
import {storage} from './config/storage';
import {baseTextController} from './message';
import {baseMethodController} from './command';
import {SheduleTimeManager} from './cron';

interface SessionData {
    name?: string;
    description?: string;
    mounth?: number;
    year?: number;
    day?: number
}

interface MyContext extends Context {
    session?: SessionData
}

const bot: Telegraf<Context<Update>> = new Telegraf(BOT_TOKEN);
bot.use(session());

//TODO переписать логику вызова на цикл, где вызывается инит
baseMethodController.init(bot);

//TODO text убрать в инит переписать логику вызова на цикл, где вызывается инит
bot.on('text', async (ctx) => await baseTextController.init(bot, ctx));

sequelize.sync().then(async () => {
    bot.launch();

    const googleSpreadSheetService = new GoogleSpreadsheetService();
    await googleSpreadSheetService.init();
    storage.spreadSheet = googleSpreadSheetService
    
    const shedule = new SheduleTimeManager({ bot });
    shedule.init();
}).catch(() => {
    console.info('Ошибка')
})




