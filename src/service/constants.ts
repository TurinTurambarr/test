const SymbolNotificationPrice: string = '*';
const MountIndex: number = 0;
const PriceIndex: number = 1;
const DiffIndex:  number = 2;

export {
    SymbolNotificationPrice, MountIndex, PriceIndex, DiffIndex
};