import { GoogleSpreadsheet, SheetType } from 'google-spreadsheet';
import { ConfigGoogleTable } from '../config/constatns';
import { parseTable } from './utils';

interface IDebtor {
    id: number;
    price: string;
    description: string;
    name: string
}

export class GoogleSpreadsheetService {
    //TODO
    readonly idGoogleTabl: string = '1JUrAZQ7W1WeoWLXbD55KKddYaoOzR7MenZdxtrnoDTo';
    readonly googleTableInstance: SheetType;
    currentSheet: any;

    constructor() {
        this.googleTableInstance = new GoogleSpreadsheet(this.idGoogleTabl);
    }

    // @ts-ignore
    async fetchListOfDebtorsByContribution() {

        const [sheet] = this.googleTableInstance.sheetsByIndex;
        await sheet.loadCells();

        const table = parseTable(sheet, {
            rowCount: 12,
            cellsCount: 14,
            startRow: 17,
        });

        return table;
    }

    // @ts-ignore
    async fetchListOfDebtorsByCollecting() {
        const [sheet] = this.googleTableInstance.sheetsByIndex;
        await sheet.loadCells();

        const table = parseTable(sheet, {
            rowCount: 12,
            cellsCount: 18,
            startRow: 0,
        });
        
        return table;
    }
    
    /**
     * login and get information on the current table
     */
    // @ts-ignore
    async init (): boolean {
        try {
            await this.googleTableInstance.useServiceAccountAuth(ConfigGoogleTable);
            await this.googleTableInstance.loadInfo();

            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

}