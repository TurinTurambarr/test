import { SymbolNotificationPrice, MountIndex, PriceIndex, DiffIndex} from "./constants";

interface IOptions {
    rowCount: number,
    cellsCount: number,
    startRow: number
}

export const parseTable = (sheet: any, {rowCount, cellsCount, startRow}: IOptions): object => {
    const table = [...new Array(rowCount)].reduce((acc, row, index) => {
        const cells = [...new Array(cellsCount)]
            .map((_, idx) => sheet.getCell(startRow + index, idx))
            .filter((cell) => cell)
            .map(({value}) => value);

        const valueCells = cells.splice(DiffIndex, cells.length);

        if(index === MountIndex) {
            acc.mouths = valueCells;
            return acc;
        }

        if(index === PriceIndex) {
            acc.priceMounth = valueCells;
            return acc;
        }

        const {value: id} = sheet.getCell(startRow + index, 0);
        acc[id] = valueCells;

        const {priceMounth, mouths} = acc;

        valueCells.forEach((value, index) => {
            const paymentMount = priceMounth[index];
            const mount = mouths[index];

            if(
                value != null
                && paymentMount != null
                && typeof value === 'string'
                && value.trim() === SymbolNotificationPrice
            ) {
                if(typeof acc.debt[id] !== 'object') {
                    acc.debt[id] = {};
                }

                acc.debt[id][mount] = paymentMount
            }

            if(value != null && value !== paymentMount && Number(value) < priceMounth[index]) {
                if(typeof acc.debt[id] !== 'object') {
                    acc.debt[id] = {};
                }

                acc.debt[id][mount] = paymentMount - Number(value);
            }
        });

        return acc;
    }, {mouth: [], priceMounth: [], noPayment: [], payment: [], debt: {}});
    
    return table;
}