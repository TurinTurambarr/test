import {MessageController, Route as MessageRoute} from './controller/MessageController';

interface IControllerOption {
    controller: object,
    route: object
}

interface IOption {
    routes: IControllerOption[]
}

class BaseTextController {
    routes: IControllerOption[];

    constructor({routes}: IOption) {
        this.routes = routes
    }

     async init(bot, ctx) {
        const {from} = ctx;
        const [command, message] = ctx.message.text.split('|');
        const currentCommand = command.trim();

        this.routes.forEach(async ({controller, route}: IControllerOption) => {
            for(let key in route) {
                const {method, command: routeCommand} = route[key];

                if(routeCommand === currentCommand) {
                    await controller[method]({ctx, message});
                }
            }
        })
    }
}

const baseTextController = new BaseTextController({
    routes: [{
        controller: MessageController,
        route: MessageRoute
    }]
});

export {
    baseTextController
}