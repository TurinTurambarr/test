import {User} from '../../model/user';
import {storage, IStorage} from '../../config/storage';
import {IRoute} from '../../command/interface';

export const Route: IRoute = {
    SAVE: {
      method: 'saveMessage',
      command: 'message'
    },
    GET_MESSAGE: {
      method: 'getMessage',
      command: 'getmessage'
    },
    INFO: {
      method: 'info',
      command: 'user'
    },
    SAVE_TITLE: {
        method: 'saveTitle',
        command: 'title'
    },
    SAVE_DESCRIPTION: {
      method: 'saveDescription',
      command: 'desc'
    },
    CHECK: {
        method: 'checkNotification',
        command: 'check'
    }
};

const SAVE_MESSAGE = `Ваше сообщение сохраненно, можете использовать его`;
const SAVE_TITLE = `Ваш заголовок сохранен`;
const SAVE_DESC = `Ваше описание сохраненно`;
const INFO_MESSAGE = `Сохраненное сообщение:`;

export class MessageController {
    public static saveMessage ({ctx, message}: any) {
        storage.message = message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_MESSAGE);
    }

    public static getMessage({ctx}: any) {
        ctx.telegram.sendMessage(ctx.from.id, `${INFO_MESSAGE} ${storage.message}`);
    }

    public static async info({ctx}: any) {
        const {from} = ctx;

        const user = await await User.findOne({
            where: {
                telegram_id: from.id
            }
        })

        // ctx.telegram.sendMessage(ctx.from.id, `Информация: ${JSON.stringify(user)}`);
    }

    public static saveTitle({ctx, message}: any) {
        storage.nootification.name = message.trim();
        ctx.session.name =  message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_TITLE);
    }

    public static saveDescription({ctx, message}: any) {
        storage.nootification.description = message.trim();
        ctx.session.description =  message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_DESC);
    }

    public static checkNotification ({ctx, message}: any) {
        const {session} = ctx;
        ctx.telegram.sendMessage(ctx.from.id, `Информация о уведомлении ${JSON.stringify(session)}`);
    }
}

