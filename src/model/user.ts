import { sequelize } from '../config/db';
import { Model, DataTypes } from 'sequelize';

export class User extends Model {}

User.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    first_name: DataTypes.STRING,
    last_name:  DataTypes.STRING,
    username: DataTypes.STRING,
    telegram_id: DataTypes.STRING,
    requestCount: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
}, { sequelize, modelName: 'user' });
