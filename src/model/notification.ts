import { sequelize } from '../config/db';
import { Model, DataTypes, Optional } from 'sequelize';
import { User } from './user';

type NotificationAttributes = {
    id: number;
    title: string;
    message: string;
    date: Date;
    sendCount: number|null
};
type NotificationCreationAttributes = Optional<NotificationAttributes, 'id'>;

export class Notification extends Model {}

Notification.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    message: DataTypes.TEXT,
    title:  DataTypes.STRING,
    date: DataTypes.DATE,
    sendCount: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
}, { sequelize, modelName: 'notification' });

User.hasOne(Notification, { onDelete: "cascade"})
