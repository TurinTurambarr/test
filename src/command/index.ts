import {Telegraf} from 'telegraf';
import {PaymentsController, Command as PaymentsCommand} from './controller/PaymentsController';
import {AuthController, Command as AuthCommand} from './controller/AuthController';
import {SendController, Command as SendCommand, Action as SendActionCallback} from './controller/SendController';
import {NotificationController, Command as NotificationCommand, Action as NotificationActionCallback}  from './controller/NotificationController';
import {merge} from 'lodash';

interface IControllerOption {
    controller: object,
    command: object
}

interface IOption {
    commands: IControllerOption[]
}

class BaseMethodController {
    commands: IControllerOption[];

    constructor({commands}: IOption = {commands: []}) {
        this.commands = commands
    }

    

    /**
     * itit command bot, call methods from Controller
     * @param {Telegraf} bot
     */
    async init(bot: Telegraf) {
         const {commands} = this;

         commands.forEach(({controller: Controller,command}) => {
             for(let key in command) {
                 const {method, name: routeCommand} = command[key];

                 bot.command(routeCommand, async (ctx) => {
                    const CommandController = new Controller();
                    await CommandController.init();
                    await CommandController[method](bot, ctx);
                 })
             }
         });
        
         // TODO пока так, продумать логику основываясь на сессиях
         const Contoller = {
             'SendController' : SendController,
             'NotificationController' : NotificationController
         }

         const Action = merge(SendActionCallback, NotificationActionCallback);

         bot.on('callback_query', async (ctx) => {
             const {data} = ctx.callbackQuery;
             const {value, callbackName} = JSON.parse(data);

             for(let key in Action) {
                 const {name, method, controller: cName} = Action[key];

                 if(callbackName.includes(name)) {
                     await Contoller[cName][method](ctx, value)
                 }
             }
         });
    }
}

const baseMethodController = new BaseMethodController({
    commands: [{
        controller: PaymentsController,
        command: PaymentsCommand
    }, {
        controller: AuthController,
        command: AuthCommand
    }, {
        controller: SendController,
        command: SendCommand
    }, {
        controller: NotificationController,
        command: NotificationCommand
    }]
});

export { baseMethodController };