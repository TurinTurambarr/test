interface IOptionRoute {
    method: string;
    command: string;
    description?: string
};

interface IOptionCommand {
    method: string;
    name: string;
    description?: string
};

interface IOptionAction {
    method: string;
    name: string;
    controller: string;
    description?: string
};

interface IRoute {
    [key: string]: IOptionRoute
};

interface ICommand {
    [key: string]: IOptionCommand
};

interface IAction {
    [key: string]: IOptionAction
};

export {
    IOptionRoute, IRoute,
    IOptionCommand, ICommand,
    IOptionAction, IAction
}