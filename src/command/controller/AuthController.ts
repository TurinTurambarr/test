import {User} from '../../model/user';
import {BaseController} from './BaseController';
// TODO interface
export const Command: object = {
    START: {
        method: 'authUser',
        name: 'start'
    },
    HELP: {
        method: 'receiveHelp',
        name: 'help'
    }
};
// TODO типы
export class AuthController extends BaseController{
  private googleSpreadSheetService: any;

  public async authUser(bot: any, ctx: any = null) {
      const {from} = ctx;

      const isActivated = await User.findOne({
          where: {
              telegram_id: from.id
          }
      });

      if(isActivated != null) {
          return ctx.reply('Ты уже зарегистрирован, ' + ctx.from.first_name + '!');
      }

      const newUser = await User.create({
          username: from.username,
          first_name: from.first_name,
          last_name: from.last_name,
          telegram_id: from.id,
          requestCount: 1
      });


      if(newUser != null) {
          return ctx.reply('Спасибо за регистрацию  ' + ctx.from.first_name + '!');
      }

      ctx.reply('Произошла ошибка попробуйте позже, ' + ctx.from.first_name + '!');
  }

  public receiveHelp(bot: any, ctx: any = null) {
      //TODO Взять из всех комманд контроллеров.
      ctx.reply('Комманда /smany поиск должников');
      ctx.reply('Комманда /sendall отправить уведомление не скольким пользователям');
      ctx.reply('Комманда /createnot создание уведомления');
      ctx.reply('Комманда /sendgroup отправить уведомление одному или нескольким из пользовотеля (выберите всех пользователей и введите комманду /send');
  }
}

