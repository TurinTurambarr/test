import {User} from '../../model/user';
import {BaseController} from './BaseController';
import {storage, IStorage} from '../../config/storage';
import {BOT_TOKEN, GOOGLE_TABLE_LINK, CARD_NUMBER, Role} from '../../config/constatns';
import {merge} from 'lodash';
// TODO interface
export const Command: object = {
    UPDATING_INFO: {
      method: 'updatingInformation',
      name: 'update',
      description: 'Обновление информации',
      role: Role.ADMIN
    },
    SEARCH_FOR_DEBTORS: {
        method: 'searchForDebts',
        name: 'sdebts',
        description: '💵 Рассылка сообщений о всех задолженностях',
        role: Role.ADMIN
    },
    SEARCH_ME_DEBTS: {
        method: 'searchMeDebts',
        name: 'medebts',
        description: '💵 Узнать свой долг'
    },
    GET_ALL_DEBTORS: {
        method: 'getAllDebtors',
        name: 'alldebts',
        description: '💵 Получить актуальную информацию о всех должниках',
        role: Role.ADMIN
    }
};
// TODO типы
export class PaymentsController extends BaseController{
  private googleSpreadSheetService: any;

  async searchForDebts(bot: any, ctx: any = null) {
      const {debt: debtContribution = null} =  await storage.spreadSheet.fetchListOfDebtorsByContribution();
      const {debt: debtCollecting = null} =  await storage.spreadSheet.fetchListOfDebtorsByCollecting();

      if(debtContribution == null || debtCollecting == null) {
          return ctx.reply('Попробуй еще раз!');
      }
      // !!!DANGER!!!  мутирует изначальные объекты
      const allDebtn = merge(debtContribution, debtCollecting);

      for(let id in allDebtn) {
          const {telegram_id} = await User.findByPk(id);
          const debtMessage = Object.entries(allDebtn[id])
              .map(mountAndPrice => mountAndPrice.join(' : '))
              .map(mountAndPrice => `За ${mountAndPrice} руб`)
              .join(', ');


          bot.telegram.sendMessage(telegram_id, `У тебя скопился долг, не забудь пополнить казну... 
💵 Долг: ${debtMessage};
📝 Таблица учетов платежей: ${GOOGLE_TABLE_LINK}; 
💳 Карта воронов: ${CARD_NUMBER}`);
      }
  }

  async getAllDebtors(bot: any, ctx: any = null) {
      ctx.reply('⏳ Идет сбор информации. ⏳');

      const {from} = ctx;
      const user: any = await User.findOne({
          where: {
              telegram_id: from.id
          }
      });

      if(user == null) {
          return ctx.reply('Тебя нет в системе!');
      }

      const {debt: debtContribution = null} =  await storage.spreadSheet.fetchListOfDebtorsByContribution();
      const {debt: debtCollecting = null} =  await storage.spreadSheet.fetchListOfDebtorsByCollecting();

      if(debtContribution == null || debtCollecting == null) {
          return ctx.reply('Попробуй еще раз!');
      }
      // !!!DANGER!!!  мутирует изначальные объекты
      const allDebtn = merge(debtContribution, debtCollecting);
      let allMessage = '';

      for(let id in allDebtn) {
          const {telegram_id, username} = await User.findByPk(id);
          const debtMessage = Object.entries(allDebtn[id])
              .map(mountAndPrice => mountAndPrice.join(' : '))
              .map(mountAndPrice => `За ${mountAndPrice} руб`)
              .join(', ');


          allMessage += `${username}:  
💵 Долг: ${debtMessage};

`;
      }

      bot.telegram.sendMessage(user.telegram_id, allMessage);
  }

  async searchMeDebts(bot: any, ctx: any = null) {
        ctx.reply('⏳ Идет сбор информации. ⏳');
        
        const {from} = ctx;
        const user: any = await User.findOne({
            where: {
                telegram_id: from.id
            }
        });

        if(user == null) {
            return ctx.reply('Тебя нет в системе!');
        }

        const {debt: debtContribution = null} =  await storage.spreadSheet.fetchListOfDebtorsByContribution();
        const {debt: debtCollecting = null} =  await storage.spreadSheet.fetchListOfDebtorsByCollecting();

        if(debtContribution == null || debtCollecting == null) {
          return ctx.reply('Попробуй еще раз!');
        }

        // !!!DANGER!!!  мутирует изначальные объекты
        const allDebtn = merge(debtContribution, debtCollecting);

        const debth: any = user.id in allDebtn ? allDebtn[user.id] : null;

        if(debth == null) {
            return ctx.reply('Долгов не найденно.');
        }

        const debtMessage = Object.entries(debth)
            .map(mountAndPrice => mountAndPrice.join(' : '))
            .map(mountAndPrice => `За ${mountAndPrice} руб`)
            .join(', ');

        bot.telegram.sendMessage(user.telegram_id, `Информация о долге.  
💵 Долг: ${debtMessage};
📝 Таблица учетов платежей: ${GOOGLE_TABLE_LINK}; 
💳 Карта воронов: ${CARD_NUMBER}`);
  }

  async updatingInformation (bot: any, ctx: any = null) {
      storage.spreadSheet.init().then(() => {
          ctx.reply('💵 Информация обновленна');
      }).catch(() => {
          ctx.reply('😳 Произошла ошибка');
      })
  }

  async init() {
      // ????? TODO инстанс получает информацию один раз при старте приложения и обновляет актуальную информацию каждый день, чтобы гугл не заблокировал из за частого обращения к таблийце
  }
}

