import {ICommand} from '../interface'

interface ICallbackData {
    value: any;
    callbackName: string
}

export class BaseController {
    // public
    //
    // constructor({command: ICommand}) {
    // }

    public init() {

    }

    public static callbackData(callbackData:ICallbackData|string, isParse: boolean = false) {
        if(isParse) {
            return JSON.parse(callbackData);
        }
        
        return JSON.stringify(callbackData);
    }
}