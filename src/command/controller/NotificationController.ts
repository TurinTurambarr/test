import {User} from '../../model/user';
import moment from 'moment';
import {Sequelize} from 'sequelize';
import {Notification} from '../../model/notification';
import {ICommand, IAction}  from '../interface/index';
import {Markup, Telegraf} from 'telegraf';
import {BaseController} from './BaseController';
import {storage, IStorage, INotification} from '../../config/storage';

// TODO
export const Command: ICommand = {
    CREATE_NOT: {
        method: 'create',
        name: 'createnot',
        description: '📝 Создание заметки',
    },
    DELETE_NOT: {
        method: 'delete',
        name: 'deletenot',
        description: '📝 Удаление заметки',
    },
    SAVE_NOT: {
        method: 'save',
        name: 'savenot',
        description: '📝 Сохранение заметки',
    }
};

export const Action: IAction = {
    CALLBACK_MOUNTH: {
        name: 'mounth',
        method: 'checkMount',
        controller: 'NotificationController'
    },
    CALLBACK_DAYS: {
        name: 'days',
        method: 'checkDays',
        controller: 'NotificationController'
    },
    CALLBACK_MESSAGE: {
        name: 'message',
        method: 'infoMessage',
        controller: 'NotificationController'
    },
    CALLBACK_NOTIFICATION_DESTROY: {
        name: 'notificationDelete',
        method: 'notificationDelete',
        controller: 'NotificationController'
    }
}

interface IMarkOption {
    text: string|number;
    callback_data: string
}

interface ICTable {
    count: number;
    name: string;
    start?: number,
    callback: any
}

// TODO типы
export class NotificationController extends BaseController{
  protected today: Date = new Date();

  public async create(bot: Telegraf, ctx: any = null) {
      const year: number = this.today.getFullYear();

      const yearInline: IMarkOption[] = NotificationController.createTableDate({count: 9, name: 'mounth', start: year});

      return ctx.telegram.sendMessage(ctx.chat.id, 'Привет, создадим уведомление! (Выбери год)',
          {
              caption: 'Caption',
              parse_mode: 'Markdown',
              ...Markup.inlineKeyboard(yearInline, {
                  columns: 3
              })
          });
  }

  public async delete(bot: Telegraf, ctx: any = null) {
    const notifications = await Notification.findAll({raw:true});

    const markdownNotification =  notifications.map((value: object, index: number) => ({
        text: `${value.date.toLocaleDateString()} : ${value.title}`,
        callback_data: BaseController.callbackData({
            value: value.id,
            callbackName: 'notificationDelete'
        })
    }));

    return ctx.telegram.sendMessage(ctx.chat.id, 'Выбери уведомление, которое хочешь удалить',
          {
              caption: 'Caption',
              parse_mode: 'Markdown',
              ...Markup.inlineKeyboard(markdownNotification, {
                  columns: 1
              })
          });
  }
  
  public async save(bot: Telegraf, ctx: any = null) {
      if(ctx.session == null) {
          return ctx.reply(`Информации нет`);
      }

      const {name: title, description, mounth, year, day} = ctx.session;
      const date: Date = new Date(year, mounth - 1, day + 1);

      const hasNotification = await Notification.create({
          message: description,
          title,
          date
      })

      return ctx.reply(hasNotification ? `Уведомление созданно` : `Уведомление не созданно`);
  }

  static async checkNotification(bot: Telegraf) {
      const notifications = await Notification.findAll({raw:true});
      const users = await User.findAll({raw: true})
      const curDate = new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString();
      const currentNotifications = notifications.filter(({date}) => date.toLocaleDateString() === curDate)

      users.forEach(({telegram_id}) => {
          currentNotifications.forEach(({title, message}) => {
              bot.telegram.sendMessage(telegram_id, `
📝 Тема: ${title}
__________________________________
${message}
`);
          })
      })
  }

  static async checkOldNotification(bot: Telegraf) {
      const notifications = await Notification.findAll({raw:true});
      const curTime = new Date().getTime();
      
      notifications.forEach(not => {
          const notDate = new Date(not.date).getTime()
          if(curTime > notDate) {
              Notification.destroy({
                  where: {
                      id: not.id
                  }
              })
          }
      })
  }


  static checkMount(ctx: any, year: number) {
      const date = new Date()
      const currentYear: number = date.getFullYear();
      const mounth: number = year === currentYear ? date.getMonth() : 12;
      const startMounth : number =  year === currentYear ? date.getMonth() + 1 : 1;
      if(ctx.session == null) {
        ctx.session = {}
      }

      storage.nootification.year = year;
      ctx.session.year = year;

      const mounthInline: IMarkOption[] = NotificationController.createTableDate({count: mounth, name: 'days', start: startMounth, callback: (mount: number) : string => {
           const mountName: string[] = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
           return mountName[mount];
      }});

      // отвечаем телеграму что получили от него запрос
      ctx.answerCbQuery();

      // удаляем сообщение
      ctx.deleteMessage();

      return ctx.telegram.sendMessage(ctx.chat.id, 'Выбери Месяц',
          {
              caption: 'Caption',
              parse_mode: 'Markdown',
              ...Markup.inlineKeyboard(mounthInline, {
                  columns: 4
              })
          });
  }

  static checkDays(ctx: any, mounth: number) {
      storage.nootification.mounth = mounth;
      ctx.session.mounth = mounth;

      const date: Date = new Date();
      const currentYear: number = date.getFullYear();
      const {year} = storage.nootification;
      const days: number = new Date(year, mounth, 0).getDate();
      const isCurrentMount : boolean = year === currentYear && mounth === (date.getMonth() + 1);
      const currentDay: number =
          isCurrentMount
              ? days - date.getDate()
              : days;

      const daysLine: IMarkOption[] = NotificationController.createTableDate(
          {
            count: isCurrentMount ? currentDay : days,
            name: 'message',
            start: isCurrentMount ? date.getDate() : 1
         }
      );

      // отвечаем телеграму что получили от него запрос
      ctx.answerCbQuery();

      // удаляем сообщение
      ctx.deleteMessage();

      return ctx.telegram.sendMessage(ctx.chat.id, 'Выбери День',
          {
              caption: 'Caption',
              parse_mode: 'Markdown',
              ...Markup.inlineKeyboard(daysLine, {
                  columns: 7
              })
          });

  }

  static async notificationDelete(ctx: any, id: number) {
      // отвечаем телеграму что получили от него запрос
      ctx.answerCbQuery();
      // удаляем сообщение
      ctx.deleteMessage();

      await Notification.destroy({
          where: { id }
      })

      return ctx.reply(`Уведомление удаленно`);
  }

  static infoMessage(ctx: any, day: number) {
        storage.nootification.day = day;

        ctx.session.day = day;
        // отвечаем телеграму что получили от него запрос
        ctx.answerCbQuery();
        // удаляем сообщение
        ctx.deleteMessage();


      return ctx.reply(`Заполните оставшиеся данные о уведомлении: 
title |  Наименование уведомления     
desc | Текст уведомления 
      `);
   }
  
  static createTableDate({count, name, start = 1, callback = null}: ICTable): IMarkOption[] {
      return [...new Array(count)].map((_, value: number) => ({
          text: callback == null ? start + value : callback(start + value - 1),
          callback_data: BaseController.callbackData({
              value: start + value,
              callbackName: name
          })
      }));
  }
}

