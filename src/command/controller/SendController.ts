import {User} from '../../model/user';
import {Markup, Telegraf} from 'telegraf';
import {BaseController} from './BaseController';
import {storage, IStorage} from '../../config/storage';
import {Role} from '../../config/constatns';
import {merge} from 'lodash';

// TODO
export const Command: object = {
    SEND_ALL: {
        method: 'sendAll',
        name: 'sendall',
        description: '📝 Рассылка уведомлений всем участникам коллектива',
    },
    SEND_GROUP: {
        method: 'sendGroup',
        name: 'sendgroup',
        description: '📝 Рассылка уведомлений выбранным участникам коллектива',
    },
    SEND: {
        method: 'send',
        name: 'send'
    }
};

export const Action: object = {
    CALLBACK_GROUP: {
        name: 'check',
        method: 'checkUser',
        controller: 'SendController'
    }
}

interface IUser {
    username: string;
    telegram_id: string
}

// TODO типы
export class SendController extends BaseController{
  protected chatId: string = "-633348847";
  public users: IUser[] = [];

  async send(bot: Telegraf, ctx: any = null) {
      const {group = [], message} = storage;
      const {users} = this;

      if(message == null || message.trim() === '') {
          ctx.reply(`Заполните сообщение используя комманду 
🗣 message | текст вашего сообщения`);
          return;
      }

      group.forEach((telegram_id) => {
          const {username} = users.find(({telegram_id: id}) => id === telegram_id);

          bot.telegram.sendMessage(telegram_id, `Уважаемый ${username}!!! ${message}`);
      });
  }

  async sendAll(bot: Telegraf, ctx: any = null) {
      const {users, chatId} = this;
      const {message = null} = storage;

      if(message == null || message.trim() === '') {
          ctx.reply(`Заполните сообщение используя комманду 
🗣 message | текст вашего сообщения`);
          return;
      }


      users.forEach((user) => {
          const {telegram_id, username} = user;
          bot.telegram.sendMessage(telegram_id, `Уважаемый ${username}!!! ${message}`);
      });

      bot.telegram.sendMessage(chatId, message);
  }

  async sendGroup(bot: Telegraf, ctx: any = null) {
      const {from} = ctx;
      const {users} = this;

      storage.userId = from.id;

      ctx.reply(
          `Выберите Воронов  🔍 и введите комманду /send для отправки сообщения!`,
          {
              caption: 'Caption',
              ...SendController.usersKey(users)
          }
      );
  }

  static async checkUser(ctx: any, idUser: string) {
      const users: any[] = await User.findAll();
      // отвечаем телеграму что получили от него запрос
      ctx.answerCbQuery();

      // удаляем сообщение
      ctx.deleteMessage();

      if(storage.group.includes(idUser)) {
          const index = storage.group.findIndex(id => id === idUser);
          storage.group.splice(index, 1);
      } else {
          storage.group.push(idUser);
      }

      // отвечаем на нажатие кнопки
      ctx.reply( `Выберите Воронов  🔍 и введите комманду /send для отправки сообщения!`, SendController.usersKey(users));
  }

  static usersKey (users: IUser[]) {
        const usersKey = users.reduce((acc: object[], user, idx) => {
            const {username, telegram_id} = user;
            const isActive = storage.group.some(id => id === telegram_id); // /${telegram_id}
            const markup = Markup.button.callback(
                `${username} ${isActive ? ' 🌶' : ''} `,
                this.callbackData({value: telegram_id, callbackName: 'check'})
            );

            if(acc.length === 0) {
                return [
                    [markup]
                ];
            }

            const lastRow: any = acc[acc.length - 1];

            if(lastRow.length === 3) {
                return [
                    ...acc,
                    [markup]
                ]
            } else {
                lastRow.push(markup)
            }

            return acc;
        }, []);

        return Markup.inlineKeyboard(usersKey);
  };

  async init () {
      this.users = await User.findAll();
  }
}

