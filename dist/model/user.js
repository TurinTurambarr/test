"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const db_1 = require("../config/db");
const sequelize_1 = require("sequelize");
class User extends sequelize_1.Model {
}
exports.User = User;
User.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    first_name: sequelize_1.DataTypes.STRING,
    last_name: sequelize_1.DataTypes.STRING,
    username: sequelize_1.DataTypes.STRING,
    telegram_id: sequelize_1.DataTypes.STRING,
    requestCount: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: true
    }
}, { sequelize: db_1.sequelize, modelName: 'user' });
//# sourceMappingURL=user.js.map