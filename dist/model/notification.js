"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Notification = void 0;
const db_1 = require("../config/db");
const sequelize_1 = require("sequelize");
const user_1 = require("./user");
class Notification extends sequelize_1.Model {
}
exports.Notification = Notification;
Notification.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    message: sequelize_1.DataTypes.TEXT,
    title: sequelize_1.DataTypes.STRING,
    date: sequelize_1.DataTypes.DATE,
    sendCount: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: true
    }
}, { sequelize: db_1.sequelize, modelName: 'notification' });
user_1.User.hasOne(Notification, { onDelete: "cascade" });
//# sourceMappingURL=notification.js.map