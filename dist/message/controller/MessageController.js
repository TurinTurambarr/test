"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageController = exports.Route = void 0;
const user_1 = require("../../model/user");
const storage_1 = require("../../config/storage");
exports.Route = {
    SAVE: {
        method: 'saveMessage',
        command: 'message'
    },
    GET_MESSAGE: {
        method: 'getMessage',
        command: 'getmessage'
    },
    INFO: {
        method: 'info',
        command: 'user'
    },
    SAVE_TITLE: {
        method: 'saveTitle',
        command: 'title'
    },
    SAVE_DESCRIPTION: {
        method: 'saveDescription',
        command: 'desc'
    },
    CHECK: {
        method: 'checkNotification',
        command: 'check'
    }
};
const SAVE_MESSAGE = `Ваше сообщение сохраненно, можете использовать его`;
const SAVE_TITLE = `Ваш заголовок сохранен`;
const SAVE_DESC = `Ваше описание сохраненно`;
const INFO_MESSAGE = `Сохраненное сообщение:`;
class MessageController {
    static saveMessage({ ctx, message }) {
        storage_1.storage.message = message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_MESSAGE);
    }
    static getMessage({ ctx }) {
        ctx.telegram.sendMessage(ctx.from.id, `${INFO_MESSAGE} ${storage_1.storage.message}`);
    }
    static async info({ ctx }) {
        const { from } = ctx;
        const user = await await user_1.User.findOne({
            where: {
                telegram_id: from.id
            }
        });
        // ctx.telegram.sendMessage(ctx.from.id, `Информация: ${JSON.stringify(user)}`);
    }
    static saveTitle({ ctx, message }) {
        storage_1.storage.nootification.name = message.trim();
        ctx.session.name = message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_TITLE);
    }
    static saveDescription({ ctx, message }) {
        storage_1.storage.nootification.description = message.trim();
        ctx.session.description = message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_DESC);
    }
    static checkNotification({ ctx, message }) {
        const { session } = ctx;
        ctx.telegram.sendMessage(ctx.from.id, `Информация о уведомлении ${JSON.stringify(session)}`);
    }
}
exports.MessageController = MessageController;
//# sourceMappingURL=MessageController.js.map