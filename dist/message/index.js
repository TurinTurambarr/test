"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.baseTextController = void 0;
const MessageController_1 = require("./controller/MessageController");
class BaseTextController {
    constructor({ routes }) {
        this.routes = routes;
    }
    async init(bot, ctx) {
        const { from } = ctx;
        const [command, message] = ctx.message.text.split('|');
        const currentCommand = command.trim();
        this.routes.forEach(async ({ controller, route }) => {
            for (let key in route) {
                const { method, command: routeCommand } = route[key];
                if (routeCommand === currentCommand) {
                    await controller[method]({ ctx, message });
                }
            }
        });
    }
}
const baseTextController = new BaseTextController({
    routes: [{
            controller: MessageController_1.MessageController,
            route: MessageController_1.Route
        }]
});
exports.baseTextController = baseTextController;
//# sourceMappingURL=index.js.map