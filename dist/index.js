"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const telegraf_1 = require("telegraf");
const GoogleSpreadsheetService_1 = require("./service/GoogleSpreadsheetService");
const constatns_1 = require("./config/constatns");
const db_1 = require("./config/db");
const storage_1 = require("./config/storage");
const message_1 = require("./message");
const command_1 = require("./command");
const cron_1 = require("./cron");
const bot = new telegraf_1.Telegraf(constatns_1.BOT_TOKEN);
bot.use((0, telegraf_1.session)());
//TODO переписать логику вызова на цикл, где вызывается инит
command_1.baseMethodController.init(bot);
//TODO text убрать в инит переписать логику вызова на цикл, где вызывается инит
bot.on('text', async (ctx) => await message_1.baseTextController.init(bot, ctx));
db_1.sequelize.sync().then(async () => {
    bot.launch();
    const googleSpreadSheetService = new GoogleSpreadsheetService_1.GoogleSpreadsheetService();
    await googleSpreadSheetService.init();
    storage_1.storage.spreadSheet = googleSpreadSheetService;
    const shedule = new cron_1.SheduleTimeManager({ bot });
    shedule.init();
}).catch(() => {
    console.info('Ошибка');
});
//# sourceMappingURL=index.js.map