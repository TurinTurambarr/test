"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseTable = void 0;
const constants_1 = require("./constants");
const parseTable = (sheet, { rowCount, cellsCount, startRow }) => {
    const table = [...new Array(rowCount)].reduce((acc, row, index) => {
        const cells = [...new Array(cellsCount)]
            .map((_, idx) => sheet.getCell(startRow + index, idx))
            .filter((cell) => cell)
            .map(({ value }) => value);
        const valueCells = cells.splice(constants_1.DiffIndex, cells.length);
        if (index === constants_1.MountIndex) {
            acc.mouths = valueCells;
            return acc;
        }
        if (index === constants_1.PriceIndex) {
            acc.priceMounth = valueCells;
            return acc;
        }
        const { value: id } = sheet.getCell(startRow + index, 0);
        acc[id] = valueCells;
        const { priceMounth, mouths } = acc;
        valueCells.forEach((value, index) => {
            const paymentMount = priceMounth[index];
            const mount = mouths[index];
            if (value != null
                && paymentMount != null
                && typeof value === 'string'
                && value.trim() === constants_1.SymbolNotificationPrice) {
                if (typeof acc.debt[id] !== 'object') {
                    acc.debt[id] = {};
                }
                acc.debt[id][mount] = paymentMount;
            }
            if (value != null && value !== paymentMount && Number(value) < priceMounth[index]) {
                if (typeof acc.debt[id] !== 'object') {
                    acc.debt[id] = {};
                }
                acc.debt[id][mount] = paymentMount - Number(value);
            }
        });
        return acc;
    }, { mouth: [], priceMounth: [], noPayment: [], payment: [], debt: {} });
    return table;
};
exports.parseTable = parseTable;
//# sourceMappingURL=utils.js.map