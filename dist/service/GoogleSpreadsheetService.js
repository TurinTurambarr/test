"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoogleSpreadsheetService = void 0;
const google_spreadsheet_1 = require("google-spreadsheet");
const constatns_1 = require("../config/constatns");
const utils_1 = require("./utils");
class GoogleSpreadsheetService {
    constructor() {
        //TODO
        this.idGoogleTabl = '1JUrAZQ7W1WeoWLXbD55KKddYaoOzR7MenZdxtrnoDTo';
        this.googleTableInstance = new google_spreadsheet_1.GoogleSpreadsheet(this.idGoogleTabl);
    }
    // @ts-ignore
    async fetchListOfDebtorsByContribution() {
        const [sheet] = this.googleTableInstance.sheetsByIndex;
        await sheet.loadCells();
        const table = (0, utils_1.parseTable)(sheet, {
            rowCount: 12,
            cellsCount: 14,
            startRow: 17,
        });
        return table;
    }
    // @ts-ignore
    async fetchListOfDebtorsByCollecting() {
        const [sheet] = this.googleTableInstance.sheetsByIndex;
        await sheet.loadCells();
        const table = (0, utils_1.parseTable)(sheet, {
            rowCount: 12,
            cellsCount: 18,
            startRow: 0,
        });
        return table;
    }
    /**
     * login and get information on the current table
     */
    // @ts-ignore
    async init() {
        try {
            await this.googleTableInstance.useServiceAccountAuth(constatns_1.ConfigGoogleTable);
            await this.googleTableInstance.loadInfo();
            return true;
        }
        catch (e) {
            console.error(e);
            return false;
        }
    }
}
exports.GoogleSpreadsheetService = GoogleSpreadsheetService;
//# sourceMappingURL=GoogleSpreadsheetService.js.map