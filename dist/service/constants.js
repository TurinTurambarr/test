"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiffIndex = exports.PriceIndex = exports.MountIndex = exports.SymbolNotificationPrice = void 0;
const SymbolNotificationPrice = '*';
exports.SymbolNotificationPrice = SymbolNotificationPrice;
const MountIndex = 0;
exports.MountIndex = MountIndex;
const PriceIndex = 1;
exports.PriceIndex = PriceIndex;
const DiffIndex = 2;
exports.DiffIndex = DiffIndex;
//# sourceMappingURL=constants.js.map