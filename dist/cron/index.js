"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SheduleTimeManager = void 0;
const node_cron_1 = __importDefault(require("node-cron"));
const storage_1 = require("../config/storage");
const PaymentsController_1 = require("../command/controller/PaymentsController");
const NotificationController_1 = require("../command/controller/NotificationController");
// каждый 3 день в 9
const THREE_DAY = '0 9 */3 * *';
const UPDATE = '0 12 * * *';
const ONE = '* * * * *';
class SheduleTimeManager {
    constructor({ time = THREE_DAY, bot } = {}) {
        this.time = time;
        this.bot = bot;
    }
    async searchDebtors() {
        const { bot } = this;
        const instancePaymentController = new PaymentsController_1.PaymentsController();
        await instancePaymentController.init();
        await instancePaymentController.searchForDebts(bot);
    }
    init() {
        const { time, bot } = this;
        node_cron_1.default.schedule(time, () => {
            ['searchDebtors'].forEach((method) => {
                this[method]();
            });
        });
        node_cron_1.default.schedule(UPDATE, () => {
            storage_1.storage.spreadSheet.init();
            NotificationController_1.NotificationController.checkNotification(bot);
            NotificationController_1.NotificationController.checkOldNotification(bot);
        });
        // test cron
        // cron.schedule(ONE, () => {
        //
        // })
    }
}
exports.SheduleTimeManager = SheduleTimeManager;
//# sourceMappingURL=index.js.map