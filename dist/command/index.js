"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.baseMethodController = void 0;
const PaymentsController_1 = require("./controller/PaymentsController");
const AuthController_1 = require("./controller/AuthController");
const SendController_1 = require("./controller/SendController");
const NotificationController_1 = require("./controller/NotificationController");
const lodash_1 = require("lodash");
class BaseMethodController {
    constructor({ commands } = { commands: [] }) {
        this.commands = commands;
    }
    /**
     * itit command bot, call methods from Controller
     * @param {Telegraf} bot
     */
    async init(bot) {
        const { commands } = this;
        commands.forEach(({ controller: Controller, command }) => {
            for (let key in command) {
                const { method, name: routeCommand } = command[key];
                bot.command(routeCommand, async (ctx) => {
                    const CommandController = new Controller();
                    await CommandController.init();
                    await CommandController[method](bot, ctx);
                });
            }
        });
        // TODO пока так, продумать логику основываясь на сессиях
        const Contoller = {
            'SendController': SendController_1.SendController,
            'NotificationController': NotificationController_1.NotificationController
        };
        const Action = (0, lodash_1.merge)(SendController_1.Action, NotificationController_1.Action);
        bot.on('callback_query', async (ctx) => {
            const { data } = ctx.callbackQuery;
            const { value, callbackName } = JSON.parse(data);
            for (let key in Action) {
                const { name, method, controller: cName } = Action[key];
                if (callbackName.includes(name)) {
                    await Contoller[cName][method](ctx, value);
                }
            }
        });
    }
}
const baseMethodController = new BaseMethodController({
    commands: [{
            controller: PaymentsController_1.PaymentsController,
            command: PaymentsController_1.Command
        }, {
            controller: AuthController_1.AuthController,
            command: AuthController_1.Command
        }, {
            controller: SendController_1.SendController,
            command: SendController_1.Command
        }, {
            controller: NotificationController_1.NotificationController,
            command: NotificationController_1.Command
        }]
});
exports.baseMethodController = baseMethodController;
//# sourceMappingURL=index.js.map