"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseController = void 0;
class BaseController {
    // public
    //
    // constructor({command: ICommand}) {
    // }
    init() {
    }
    static callbackData(callbackData, isParse = false) {
        if (isParse) {
            return JSON.parse(callbackData);
        }
        return JSON.stringify(callbackData);
    }
}
exports.BaseController = BaseController;
//# sourceMappingURL=BaseController.js.map