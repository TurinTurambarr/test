"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentsController = exports.Command = void 0;
const user_1 = require("../../model/user");
const BaseController_1 = require("./BaseController");
const storage_1 = require("../../config/storage");
const constatns_1 = require("../../config/constatns");
const lodash_1 = require("lodash");
// TODO interface
exports.Command = {
    UPDATING_INFO: {
        method: 'updatingInformation',
        name: 'update',
        description: 'Обновление информации',
        role: constatns_1.Role.ADMIN
    },
    SEARCH_FOR_DEBTORS: {
        method: 'searchForDebts',
        name: 'sdebts',
        description: '💵 Рассылка сообщений о всех задолженностях',
        role: constatns_1.Role.ADMIN
    },
    SEARCH_ME_DEBTS: {
        method: 'searchMeDebts',
        name: 'medebts',
        description: '💵 Узнать свой долг'
    },
    GET_ALL_DEBTORS: {
        method: 'getAllDebtors',
        name: 'alldebts',
        description: '💵 Получить актуальную информацию о всех должниках',
        role: constatns_1.Role.ADMIN
    }
};
// TODO типы
class PaymentsController extends BaseController_1.BaseController {
    async searchForDebts(bot, ctx = null) {
        const { debt: debtContribution = null } = await storage_1.storage.spreadSheet.fetchListOfDebtorsByContribution();
        const { debt: debtCollecting = null } = await storage_1.storage.spreadSheet.fetchListOfDebtorsByCollecting();
        if (debtContribution == null || debtCollecting == null) {
            return ctx.reply('Попробуй еще раз!');
        }
        // !!!DANGER!!!  мутирует изначальные объекты
        const allDebtn = (0, lodash_1.merge)(debtContribution, debtCollecting);
        for (let id in allDebtn) {
            const { telegram_id } = await user_1.User.findByPk(id);
            const debtMessage = Object.entries(allDebtn[id])
                .map(mountAndPrice => mountAndPrice.join(' : '))
                .map(mountAndPrice => `За ${mountAndPrice} руб`)
                .join(', ');
            bot.telegram.sendMessage(telegram_id, `У тебя скопился долг, не забудь пополнить казну... 
💵 Долг: ${debtMessage};
📝 Таблица учетов платежей: ${constatns_1.GOOGLE_TABLE_LINK}; 
💳 Карта воронов: ${constatns_1.CARD_NUMBER}`);
        }
    }
    async getAllDebtors(bot, ctx = null) {
        ctx.reply('⏳ Идет сбор информации. ⏳');
        const { from } = ctx;
        const user = await user_1.User.findOne({
            where: {
                telegram_id: from.id
            }
        });
        if (user == null) {
            return ctx.reply('Тебя нет в системе!');
        }
        const { debt: debtContribution = null } = await storage_1.storage.spreadSheet.fetchListOfDebtorsByContribution();
        const { debt: debtCollecting = null } = await storage_1.storage.spreadSheet.fetchListOfDebtorsByCollecting();
        if (debtContribution == null || debtCollecting == null) {
            return ctx.reply('Попробуй еще раз!');
        }
        // !!!DANGER!!!  мутирует изначальные объекты
        const allDebtn = (0, lodash_1.merge)(debtContribution, debtCollecting);
        let allMessage = '';
        for (let id in allDebtn) {
            const { telegram_id, username } = await user_1.User.findByPk(id);
            const debtMessage = Object.entries(allDebtn[id])
                .map(mountAndPrice => mountAndPrice.join(' : '))
                .map(mountAndPrice => `За ${mountAndPrice} руб`)
                .join(', ');
            allMessage += `${username}:  
💵 Долг: ${debtMessage};

`;
        }
        bot.telegram.sendMessage(user.telegram_id, allMessage);
    }
    async searchMeDebts(bot, ctx = null) {
        ctx.reply('⏳ Идет сбор информации. ⏳');
        const { from } = ctx;
        const user = await user_1.User.findOne({
            where: {
                telegram_id: from.id
            }
        });
        if (user == null) {
            return ctx.reply('Тебя нет в системе!');
        }
        const { debt: debtContribution = null } = await storage_1.storage.spreadSheet.fetchListOfDebtorsByContribution();
        const { debt: debtCollecting = null } = await storage_1.storage.spreadSheet.fetchListOfDebtorsByCollecting();
        if (debtContribution == null || debtCollecting == null) {
            return ctx.reply('Попробуй еще раз!');
        }
        // !!!DANGER!!!  мутирует изначальные объекты
        const allDebtn = (0, lodash_1.merge)(debtContribution, debtCollecting);
        const debth = user.id in allDebtn ? allDebtn[user.id] : null;
        if (debth == null) {
            return ctx.reply('Долгов не найденно.');
        }
        const debtMessage = Object.entries(debth)
            .map(mountAndPrice => mountAndPrice.join(' : '))
            .map(mountAndPrice => `За ${mountAndPrice} руб`)
            .join(', ');
        bot.telegram.sendMessage(user.telegram_id, `Информация о долге.  
💵 Долг: ${debtMessage};
📝 Таблица учетов платежей: ${constatns_1.GOOGLE_TABLE_LINK}; 
💳 Карта воронов: ${constatns_1.CARD_NUMBER}`);
    }
    async updatingInformation(bot, ctx = null) {
        storage_1.storage.spreadSheet.init().then(() => {
            ctx.reply('💵 Информация обновленна');
        }).catch(() => {
            ctx.reply('😳 Произошла ошибка');
        });
    }
    async init() {
        // ????? TODO инстанс получает информацию один раз при старте приложения и обновляет актуальную информацию каждый день, чтобы гугл не заблокировал из за частого обращения к таблийце
    }
}
exports.PaymentsController = PaymentsController;
//# sourceMappingURL=PaymentsController.js.map