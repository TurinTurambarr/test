"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = exports.Command = void 0;
const user_1 = require("../../model/user");
const BaseController_1 = require("./BaseController");
// TODO interface
exports.Command = {
    START: {
        method: 'authUser',
        name: 'start'
    },
    HELP: {
        method: 'receiveHelp',
        name: 'help'
    }
};
// TODO типы
class AuthController extends BaseController_1.BaseController {
    async authUser(bot, ctx = null) {
        const { from } = ctx;
        const isActivated = await user_1.User.findOne({
            where: {
                telegram_id: from.id
            }
        });
        if (isActivated != null) {
            return ctx.reply('Ты уже зарегистрирован, ' + ctx.from.first_name + '!');
        }
        const newUser = await user_1.User.create({
            username: from.username,
            first_name: from.first_name,
            last_name: from.last_name,
            telegram_id: from.id,
            requestCount: 1
        });
        if (newUser != null) {
            return ctx.reply('Спасибо за регистрацию  ' + ctx.from.first_name + '!');
        }
        ctx.reply('Произошла ошибка попробуйте позже, ' + ctx.from.first_name + '!');
    }
    receiveHelp(bot, ctx = null) {
        //TODO Взять из всех комманд контроллеров.
        ctx.reply('Комманда /smany поиск должников');
        ctx.reply('Комманда /sendall отправить уведомление не скольким пользователям');
        ctx.reply('Комманда /createnot создание уведомления');
        ctx.reply('Комманда /sendgroup отправить уведомление одному или нескольким из пользовотеля (выберите всех пользователей и введите комманду /send');
    }
}
exports.AuthController = AuthController;
//# sourceMappingURL=AuthController.js.map