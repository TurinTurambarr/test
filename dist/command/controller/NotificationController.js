"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationController = exports.Action = exports.Command = void 0;
const user_1 = require("../../model/user");
const notification_1 = require("../../model/notification");
const telegraf_1 = require("telegraf");
const BaseController_1 = require("./BaseController");
const storage_1 = require("../../config/storage");
// TODO
exports.Command = {
    CREATE_NOT: {
        method: 'create',
        name: 'createnot',
        description: '📝 Создание заметки',
    },
    DELETE_NOT: {
        method: 'delete',
        name: 'deletenot',
        description: '📝 Удаление заметки',
    },
    SAVE_NOT: {
        method: 'save',
        name: 'savenot',
        description: '📝 Сохранение заметки',
    }
};
exports.Action = {
    CALLBACK_MOUNTH: {
        name: 'mounth',
        method: 'checkMount',
        controller: 'NotificationController'
    },
    CALLBACK_DAYS: {
        name: 'days',
        method: 'checkDays',
        controller: 'NotificationController'
    },
    CALLBACK_MESSAGE: {
        name: 'message',
        method: 'infoMessage',
        controller: 'NotificationController'
    },
    CALLBACK_NOTIFICATION_DESTROY: {
        name: 'notificationDelete',
        method: 'notificationDelete',
        controller: 'NotificationController'
    }
};
// TODO типы
class NotificationController extends BaseController_1.BaseController {
    constructor() {
        super(...arguments);
        this.today = new Date();
    }
    async create(bot, ctx = null) {
        const year = this.today.getFullYear();
        const yearInline = NotificationController.createTableDate({ count: 9, name: 'mounth', start: year });
        return ctx.telegram.sendMessage(ctx.chat.id, 'Привет, создадим уведомление! (Выбери год)', {
            caption: 'Caption',
            parse_mode: 'Markdown',
            ...telegraf_1.Markup.inlineKeyboard(yearInline, {
                columns: 3
            })
        });
    }
    async delete(bot, ctx = null) {
        const notifications = await notification_1.Notification.findAll({ raw: true });
        const markdownNotification = notifications.map((value, index) => ({
            text: `${value.date.toLocaleDateString()} : ${value.title}`,
            callback_data: BaseController_1.BaseController.callbackData({
                value: value.id,
                callbackName: 'notificationDelete'
            })
        }));
        return ctx.telegram.sendMessage(ctx.chat.id, 'Выбери уведомление, которое хочешь удалить', {
            caption: 'Caption',
            parse_mode: 'Markdown',
            ...telegraf_1.Markup.inlineKeyboard(markdownNotification, {
                columns: 1
            })
        });
    }
    async save(bot, ctx = null) {
        if (ctx.session == null) {
            return ctx.reply(`Информации нет`);
        }
        const { name: title, description, mounth, year, day } = ctx.session;
        const date = new Date(year, mounth - 1, day + 1);
        const hasNotification = await notification_1.Notification.create({
            message: description,
            title,
            date
        });
        return ctx.reply(hasNotification ? `Уведомление созданно` : `Уведомление не созданно`);
    }
    static async checkNotification(bot) {
        const notifications = await notification_1.Notification.findAll({ raw: true });
        const users = await user_1.User.findAll({ raw: true });
        const curDate = new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString();
        const currentNotifications = notifications.filter(({ date }) => date.toLocaleDateString() === curDate);
        users.forEach(({ telegram_id }) => {
            currentNotifications.forEach(({ title, message }) => {
                bot.telegram.sendMessage(telegram_id, `
📝 Тема: ${title}
__________________________________
${message}
`);
            });
        });
    }
    static async checkOldNotification(bot) {
        const notifications = await notification_1.Notification.findAll({ raw: true });
        const curTime = new Date().getTime();
        notifications.forEach(not => {
            const notDate = new Date(not.date).getTime();
            if (curTime > notDate) {
                notification_1.Notification.destroy({
                    where: {
                        id: not.id
                    }
                });
            }
        });
    }
    static checkMount(ctx, year) {
        const date = new Date();
        const currentYear = date.getFullYear();
        const mounth = year === currentYear ? date.getMonth() : 12;
        const startMounth = year === currentYear ? date.getMonth() + 1 : 1;
        if (ctx.session == null) {
            ctx.session = {};
        }
        storage_1.storage.nootification.year = year;
        ctx.session.year = year;
        const mounthInline = NotificationController.createTableDate({ count: mounth, name: 'days', start: startMounth, callback: (mount) => {
                const mountName = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
                return mountName[mount];
            } });
        // отвечаем телеграму что получили от него запрос
        ctx.answerCbQuery();
        // удаляем сообщение
        ctx.deleteMessage();
        return ctx.telegram.sendMessage(ctx.chat.id, 'Выбери Месяц', {
            caption: 'Caption',
            parse_mode: 'Markdown',
            ...telegraf_1.Markup.inlineKeyboard(mounthInline, {
                columns: 4
            })
        });
    }
    static checkDays(ctx, mounth) {
        storage_1.storage.nootification.mounth = mounth;
        ctx.session.mounth = mounth;
        const date = new Date();
        const currentYear = date.getFullYear();
        const { year } = storage_1.storage.nootification;
        const days = new Date(year, mounth, 0).getDate();
        const isCurrentMount = year === currentYear && mounth === (date.getMonth() + 1);
        const currentDay = isCurrentMount
            ? days - date.getDate()
            : days;
        const daysLine = NotificationController.createTableDate({
            count: isCurrentMount ? currentDay : days,
            name: 'message',
            start: isCurrentMount ? date.getDate() : 1
        });
        // отвечаем телеграму что получили от него запрос
        ctx.answerCbQuery();
        // удаляем сообщение
        ctx.deleteMessage();
        return ctx.telegram.sendMessage(ctx.chat.id, 'Выбери День', {
            caption: 'Caption',
            parse_mode: 'Markdown',
            ...telegraf_1.Markup.inlineKeyboard(daysLine, {
                columns: 7
            })
        });
    }
    static async notificationDelete(ctx, id) {
        // отвечаем телеграму что получили от него запрос
        ctx.answerCbQuery();
        // удаляем сообщение
        ctx.deleteMessage();
        await notification_1.Notification.destroy({
            where: { id }
        });
        return ctx.reply(`Уведомление удаленно`);
    }
    static infoMessage(ctx, day) {
        storage_1.storage.nootification.day = day;
        ctx.session.day = day;
        // отвечаем телеграму что получили от него запрос
        ctx.answerCbQuery();
        // удаляем сообщение
        ctx.deleteMessage();
        return ctx.reply(`Заполните оставшиеся данные о уведомлении: 
title |  Наименование уведомления     
desc | Текст уведомления 
      `);
    }
    static createTableDate({ count, name, start = 1, callback = null }) {
        return [...new Array(count)].map((_, value) => ({
            text: callback == null ? start + value : callback(start + value - 1),
            callback_data: BaseController_1.BaseController.callbackData({
                value: start + value,
                callbackName: name
            })
        }));
    }
}
exports.NotificationController = NotificationController;
//# sourceMappingURL=NotificationController.js.map