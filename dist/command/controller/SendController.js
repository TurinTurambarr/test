"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendController = exports.Action = exports.Command = void 0;
const user_1 = require("../../model/user");
const telegraf_1 = require("telegraf");
const BaseController_1 = require("./BaseController");
const storage_1 = require("../../config/storage");
// TODO
exports.Command = {
    SEND_ALL: {
        method: 'sendAll',
        name: 'sendall',
        description: '📝 Рассылка уведомлений всем участникам коллектива',
    },
    SEND_GROUP: {
        method: 'sendGroup',
        name: 'sendgroup',
        description: '📝 Рассылка уведомлений выбранным участникам коллектива',
    },
    SEND: {
        method: 'send',
        name: 'send'
    }
};
exports.Action = {
    CALLBACK_GROUP: {
        name: 'check',
        method: 'checkUser',
        controller: 'SendController'
    }
};
// TODO типы
class SendController extends BaseController_1.BaseController {
    constructor() {
        super(...arguments);
        this.chatId = "-633348847";
        this.users = [];
    }
    async send(bot, ctx = null) {
        const { group = [], message } = storage_1.storage;
        const { users } = this;
        if (message == null || message.trim() === '') {
            ctx.reply(`Заполните сообщение используя комманду 
🗣 message | текст вашего сообщения`);
            return;
        }
        group.forEach((telegram_id) => {
            const { username } = users.find(({ telegram_id: id }) => id === telegram_id);
            bot.telegram.sendMessage(telegram_id, `Уважаемый ${username}!!! ${message}`);
        });
    }
    async sendAll(bot, ctx = null) {
        const { users, chatId } = this;
        const { message = null } = storage_1.storage;
        if (message == null || message.trim() === '') {
            ctx.reply(`Заполните сообщение используя комманду 
🗣 message | текст вашего сообщения`);
            return;
        }
        users.forEach((user) => {
            const { telegram_id, username } = user;
            bot.telegram.sendMessage(telegram_id, `Уважаемый ${username}!!! ${message}`);
        });
        bot.telegram.sendMessage(chatId, message);
    }
    async sendGroup(bot, ctx = null) {
        const { from } = ctx;
        const { users } = this;
        storage_1.storage.userId = from.id;
        ctx.reply(`Выберите Воронов  🔍 и введите комманду /send для отправки сообщения!`, {
            caption: 'Caption',
            ...SendController.usersKey(users)
        });
    }
    static async checkUser(ctx, idUser) {
        const users = await user_1.User.findAll();
        // отвечаем телеграму что получили от него запрос
        ctx.answerCbQuery();
        // удаляем сообщение
        ctx.deleteMessage();
        if (storage_1.storage.group.includes(idUser)) {
            const index = storage_1.storage.group.findIndex(id => id === idUser);
            storage_1.storage.group.splice(index, 1);
        }
        else {
            storage_1.storage.group.push(idUser);
        }
        // отвечаем на нажатие кнопки
        ctx.reply(`Выберите Воронов  🔍 и введите комманду /send для отправки сообщения!`, SendController.usersKey(users));
    }
    static usersKey(users) {
        const usersKey = users.reduce((acc, user, idx) => {
            const { username, telegram_id } = user;
            const isActive = storage_1.storage.group.some(id => id === telegram_id); // /${telegram_id}
            const markup = telegraf_1.Markup.button.callback(`${username} ${isActive ? ' 🌶' : ''} `, this.callbackData({ value: telegram_id, callbackName: 'check' }));
            if (acc.length === 0) {
                return [
                    [markup]
                ];
            }
            const lastRow = acc[acc.length - 1];
            if (lastRow.length === 3) {
                return [
                    ...acc,
                    [markup]
                ];
            }
            else {
                lastRow.push(markup);
            }
            return acc;
        }, []);
        return telegraf_1.Markup.inlineKeyboard(usersKey);
    }
    ;
    async init() {
        this.users = await user_1.User.findAll();
    }
}
exports.SendController = SendController;
//# sourceMappingURL=SendController.js.map