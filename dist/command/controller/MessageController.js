"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageController = exports.Command = void 0;
const user_1 = require("../../model/user");
const storage_1 = require("../../config/storage");
exports.Command = {
    SAVE: {
        method: 'saveMessage',
        command: 'message'
    }
};
const SAVE_MESSAGE = `Ваше сообщение сохраненно, можете использовать его для рассылки`;
const INFO_MESSAGE = `Сохраненное сообщение:`;
class MessageController {
    static saveMessage({ ctx, message }) {
        storage_1.storage.message = message.trim();
        ctx.telegram.sendMessage(ctx.from.id, SAVE_MESSAGE);
    }
    static getMessage({ ctx }) {
        ctx.telegram.sendMessage(ctx.from.id, `${INFO_MESSAGE} ${storage_1.storage.message}`);
    }
    static async info({ ctx }) {
        const { from } = ctx;
        const user = await await user_1.User.findOne({
            where: {
                telegram_id: from.id
            }
        });
        // ctx.telegram.sendMessage(ctx.from.id, `Информация: ${JSON.stringify(user)}`);
    }
}
exports.MessageController = MessageController;
//# sourceMappingURL=MessageController.js.map